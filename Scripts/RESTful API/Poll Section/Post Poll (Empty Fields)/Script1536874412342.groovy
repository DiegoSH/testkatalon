import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('RESTful API/Poll Section/Post Poll', [('full_name') : '', ('email') : '', ('job') : ''
            , ('event_id') : '']))

WS.verifyResponseStatusCode(response, 422)

WS.verifyElementPropertyValue(response, 'errors.full_name[0]', 'El campo full name es obligatorio.')

WS.verifyElementPropertyValue(response, 'errors.email[0]', 'El campo correo electrónico es obligatorio.')

WS.verifyElementPropertyValue(response, 'errors.job[0]', 'El campo job es obligatorio.')

WS.verifyElementPropertyValue(response, 'errors.event_id[0]', 'El campo event id es obligatorio.')

