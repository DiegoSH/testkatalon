<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ValidationsApiQuestions</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-13T14:40:36</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2ef66897-d0af-4fa7-bf2b-2f7bee371402</testSuiteGuid>
   <testCaseLink>
      <guid>b4585236-3aff-4b85-8598-b0f899ab9659</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Get Questions if event id is valid (Survey not available)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7a2ca77-787b-4e30-8f22-f2df08bfd17a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Get questions if event id is valid (Non-existent event)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ce0bf40-caf7-413a-83a7-c793a8e35e9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Get questions if event id is valid (Successful)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
