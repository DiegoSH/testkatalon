<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ValidationsApiPoll</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>10481e64-2791-4870-83ad-8700836d3209</testSuiteGuid>
   <testCaseLink>
      <guid>ce865870-4e3d-49e9-9404-a9183493e237</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (Email Field Empty)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9264874a-5664-422e-800b-b83791fed690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (Email was not valid)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b0fbffc-44d6-43e8-a411-26752e879aba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (Empty Fields)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d87373b-5c32-484d-92ca-dc50d843c734</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (event_id Field Empty)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cebe2bd9-71d4-4137-b74c-2c9b85d869a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (Full Name Field Empty)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f787d34-7b8e-4575-97b2-8920bae93815</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RESTful API/Poll Section/Post Poll (Job Field Empty)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
